﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GSB_CR
{
    public partial class Form5 : Form
    {
        MySqlConnection connexion = new MySqlConnection("database=gsb; server=localhost; uid=root; Pwd=; SslMode=none;");
        string [] pra_Pre_Nom;
        public Form5(string pra_Nom_prenom)
        {
            InitializeComponent();
            pra_Pre_Nom = pra_Nom_prenom.Split(' ');
        }

        private void Form5_Load(object sender, EventArgs e)
        {
            try
            {
                //On voit si la connexion fonctionne
                connexion.Open();
                MySqlCommand cmd;
                MySqlDataReader reader;

                string sql = "SELECT * FROM `praticien` WHERE PRA_NOM = '" + pra_Pre_Nom[0] +"'";
                cmd = new MySqlCommand(sql, connexion);
                reader = cmd.ExecuteReader();

                reader.Read();
                tb_Pra_Id.Text = reader[0].ToString();
                tb_Pra_Nom.Text = reader[1].ToString();
                tb_Pra_Prenom.Text = reader[2].ToString();
                tb_Pra_Adresse.Text = reader[3].ToString();
                tb_Pra_Ville_CP.Text = reader[4].ToString() + "( " + reader[5].ToString() + " )";
                tb_Pra_Notoriete.Text = reader[6].ToString();
                tb_Pra_Visite.Text = reader[7].ToString();
                tb_Pra_Code.Text = reader[8].ToString();
                reader.Close();

            }
            catch (MySqlException co)
            {
                //En cas d'erreur, code d'erreur + Message perso
                MessageBox.Show(co.ToString());
                MessageBox.Show("Désolé la connexion n'a pas fonctionné");
            }
        }

        private void btn_CR_Fermer_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
