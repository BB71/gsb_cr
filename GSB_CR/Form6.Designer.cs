﻿namespace GSB_CR
{
    partial class Form6
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tb_NewCR_NumRap = new System.Windows.Forms.TextBox();
            this.tb_NewCR_Date = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.cb_NewCR_NomColab = new System.Windows.Forms.ComboBox();
            this.cb_NewCR_NomPra = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.rtb_NewCR_RapBilan = new System.Windows.Forms.RichTextBox();
            this.rtb_NewCR_RapMotif = new System.Windows.Forms.RichTextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.btn_NewCR_Creer = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(101, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Nom Colaborateur : ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 68);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(79, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Nom Praticien :";
            // 
            // tb_NewCR_NumRap
            // 
            this.tb_NewCR_NumRap.Location = new System.Drawing.Point(124, 39);
            this.tb_NewCR_NumRap.Name = "tb_NewCR_NumRap";
            this.tb_NewCR_NumRap.Size = new System.Drawing.Size(121, 20);
            this.tb_NewCR_NumRap.TabIndex = 4;
            // 
            // tb_NewCR_Date
            // 
            this.tb_NewCR_Date.Location = new System.Drawing.Point(124, 92);
            this.tb_NewCR_Date.Name = "tb_NewCR_Date";
            this.tb_NewCR_Date.Size = new System.Drawing.Size(121, 20);
            this.tb_NewCR_Date.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 42);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(106, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Numéro du Rapport :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 95);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(36, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Date :";
            // 
            // cb_NewCR_NomColab
            // 
            this.cb_NewCR_NomColab.FormattingEnabled = true;
            this.cb_NewCR_NomColab.Location = new System.Drawing.Point(124, 12);
            this.cb_NewCR_NomColab.Name = "cb_NewCR_NomColab";
            this.cb_NewCR_NomColab.Size = new System.Drawing.Size(121, 21);
            this.cb_NewCR_NomColab.TabIndex = 8;
            // 
            // cb_NewCR_NomPra
            // 
            this.cb_NewCR_NomPra.FormattingEnabled = true;
            this.cb_NewCR_NomPra.Location = new System.Drawing.Point(124, 65);
            this.cb_NewCR_NomPra.Name = "cb_NewCR_NomPra";
            this.cb_NewCR_NomPra.Size = new System.Drawing.Size(121, 21);
            this.cb_NewCR_NomPra.TabIndex = 9;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 124);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(92, 13);
            this.label5.TabIndex = 11;
            this.label5.Text = "Bilan du Rapport :";
            // 
            // rtb_NewCR_RapBilan
            // 
            this.rtb_NewCR_RapBilan.Location = new System.Drawing.Point(124, 121);
            this.rtb_NewCR_RapBilan.Name = "rtb_NewCR_RapBilan";
            this.rtb_NewCR_RapBilan.Size = new System.Drawing.Size(175, 100);
            this.rtb_NewCR_RapBilan.TabIndex = 12;
            this.rtb_NewCR_RapBilan.Text = "";
            // 
            // rtb_NewCR_RapMotif
            // 
            this.rtb_NewCR_RapMotif.Location = new System.Drawing.Point(124, 227);
            this.rtb_NewCR_RapMotif.Name = "rtb_NewCR_RapMotif";
            this.rtb_NewCR_RapMotif.Size = new System.Drawing.Size(175, 44);
            this.rtb_NewCR_RapMotif.TabIndex = 14;
            this.rtb_NewCR_RapMotif.Text = "";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(12, 230);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(92, 13);
            this.label6.TabIndex = 13;
            this.label6.Text = "Motif du Rapport :";
            // 
            // btn_NewCR_Creer
            // 
            this.btn_NewCR_Creer.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.btn_NewCR_Creer.Location = new System.Drawing.Point(124, 277);
            this.btn_NewCR_Creer.Name = "btn_NewCR_Creer";
            this.btn_NewCR_Creer.Size = new System.Drawing.Size(91, 34);
            this.btn_NewCR_Creer.TabIndex = 15;
            this.btn_NewCR_Creer.Text = "Créer";
            this.btn_NewCR_Creer.UseVisualStyleBackColor = true;
            this.btn_NewCR_Creer.Click += new System.EventHandler(this.button1_Click);
            // 
            // Form6
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(309, 317);
            this.Controls.Add(this.btn_NewCR_Creer);
            this.Controls.Add(this.rtb_NewCR_RapMotif);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.rtb_NewCR_RapBilan);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.cb_NewCR_NomPra);
            this.Controls.Add(this.cb_NewCR_NomColab);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.tb_NewCR_Date);
            this.Controls.Add(this.tb_NewCR_NumRap);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "Form6";
            this.Text = "Création d\'un Compte Rendu";
            this.Load += new System.EventHandler(this.Form6_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tb_NewCR_NumRap;
        private System.Windows.Forms.TextBox tb_NewCR_Date;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cb_NewCR_NomColab;
        private System.Windows.Forms.ComboBox cb_NewCR_NomPra;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.RichTextBox rtb_NewCR_RapBilan;
        private System.Windows.Forms.RichTextBox rtb_NewCR_RapMotif;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btn_NewCR_Creer;
    }
}